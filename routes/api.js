
/*
* node API to handle calls made from frontend(angular)
* */

// grab the nerd model we just created
var model = require('./db-model');

module.exports = function(app) {
    // server routes ===========================================================
    // handle things like api calls
    // authentication routes

    // sample api route
    app.post('/api/message', function(req, res) {
        // use mongoose to get all nerds in the database
        //var query  = new model.query(req.body);
        //console.log(query);
        //query.save(function(err, query) {
        model.query.create(req.body, function(err, query) {

            // if there is an error retrieving, send the error.
            // nothing after res.send(err) will execute
            console.log('<******** RESPONSE ********>');
            console.log(err);
            console.log('<******** RESPONSE ********>');
            console.log(query);
            if (err)
                res.send({success:0, message: err});
            res.send({success:1, message: 'Thanks for your message. We will revert you back shortly.'});
        });
    });

    // route to handle creating goes here (app.post)
    // route to handle delete goes here (app.delete)

    // frontend routes =========================================================
    // route to handle all angular requests
    app.get('*', function(req, res) {
        res.sendfile('./public/index.html');
    });

};