
/*
* db connection and model definition
* */

// grab the mongoose module
var mongoose = require('mongoose');
// grab config file
var config = require('./config');

// mongoose connection to mongodb
mongoose.connect(config.url, function (error) {
    if (error) {
        console.log(error);
    }
});
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
mongoose.connection.once('open', function callback () {
    console.log('connection open');
});
mongoose.connection.on('disconnected', function () {
    mongoose.connect(config.url);
})

// mongoose schema definition
var mongooseSchema = mongoose.Schema;
var querySchema = new mongooseSchema({
    name: {type : String, default: ''},
    email: {type : String, default: ''},
    description: {type : String, default: ''}
});

// define our model
// module.exports allows us to pass this to other files when it is called
/*
// 1st approach
module.exports = mongoose.model('user', {
    name : {type : String, default: ''}
});
*/
// 2nd approach
module.exports = {
    query: mongoose.model('query-model', querySchema)
}

