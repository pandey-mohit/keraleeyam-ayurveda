
/*
 * about controller
 * */
angular.module('aboutController', []).controller('aboutController',['$scope', '$location', '$anchorScroll', function($scope, $location, $anchorScroll) {

    // create local instance of utility object
    $scope.util = util;

    // move scroll position to passed id
    $scope.moveTo = function(id){
        // set hash location to clicked id so that isActive class is added to it.
        $location.hash(id);
        // scroll to element id
        $scope.util.scrollTo(id);
    }

    // set active class to right hand navigation on route change
    $scope.isActive = function(route){
        return route === $location.hash();
    }

    // if hash is available, scroll to hash position on page reload
    if(!$scope.util.isNull($location.hash()))
        $anchorScroll($location.hash());
        //$scope.util.scrollTo($location.hash());

}]);