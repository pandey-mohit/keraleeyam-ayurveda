
/*
 * contact controller
 * */
angular.module('contactController', ['contactServices']).controller('contactController',['$scope', '$timeout', 'service', function($scope, $timeout, service) {

    $scope.save = function() {
        service.post($scope.user).success(function(response) {
            console.log(response);
            if(response.success == 1){
                $scope.showResponseMessage('success', response.message);
                $scope.user = {};
                $scope.queryForm.$setPristine(true);
            } else {
                $scope.showResponseMessage('error', response.message);
                $response = true;
            }
        }).error(function(error){
            $scope.showResponseMessage('success', error);
        });
    };

    $scope.interval = 5000;
    //$scope.successResponse = false;
    //$scope.errorResponse = false;
    //$scope.successResponseMessage = '';
    //$scope.errorResponseMessage = '';
    $scope.showResponseMessage = function(status, message){
        if(status == 'success'){
            $scope.successResponse = true;
            $scope.successResponseMessage = message;
            $timeout(function(){
                $scope.successResponse = false;
            },$scope.interval);
        } else {
            $scope.errorResponse = true;
            $scope.errorResponseMessage = message;
            $timeout(function(){
                $scope.errorResponse = false;
            },$scope.interval);
        }
    }

}]);