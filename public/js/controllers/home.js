
/*
 * home controller
 * */
angular.module('homeController', ['ui.bootstrap']).controller('homeController',['$scope', function($scope) {
    $scope.interval = 5000;
    $scope.slides = [{
        image: 'img/banner/banner_01.jpg',
        heading: 'Ayurveda Keraleeyam',
        content: 'Rejunevate Yourself!!!'
    }, {
        image: 'img/banner/banner_02.jpg',
        heading: 'Ayurveda Keraleeyam',
        content: 'Rejunevate Yourself!!!'
    }, {
        image: 'img/banner/banner_03.jpg',
        heading: 'Ayurveda Keraleeyam',
        content: 'Rejunevate Yourself!!!'
    }, {
        image: 'img/banner/banner_04.jpg',
        heading: 'Ayurveda Keraleeyam',
        content: 'Rejunevate Yourself!!!'
    }];

}]);