
/*
* application start point
* starting up all required component using dependency injection
* */
angular.module('ayurveda', ['ngRoute', 'routeProvider', 'homeController', 'aboutController', 'contactController']);