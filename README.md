# MEAN FullStack

This repo contains a demo project using MEAN(MongoDB + ExpressJS + AngularJS + NodeJS) FullStack JS Framework.


## Prerequisites

You need to install node.js and express.js(~4.9.0) to run this project.

1. Visit http://nodejs.org/

2. Install node to your system.

3. Open cmd(for windows) / terminal(for mac)

4. Type
    ```
     npm install -g express-generator
     ```
    and press ENTER. (It will install express framework).

5. If error occurred in step 4, try
    ```
     npm install -g express-generator
     ```
    It asks for password (admin/superuser permission).

6. Once express is installed, move to application directory and type
    ```
    node app
    ```


